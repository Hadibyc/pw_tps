<?php 
require_once("/home/chemsou/Bureau/Polytech-2020/S2/PWEB/pw_tps/Exercices/TP2/Points d’accès wifi/Q1-Q7.php");

function set_array_adresses_distances2($bornes ,$coords){
     $i=0;
    foreach ($bornes as $borne)
    {
        $bornes[$i]=[
        'id' => $borne['id'],
		'numcr' => $borne['numcr'],
		'nums' => $borne['nums'],
		'microcell' => $borne['microcell'],
		'adr' => $borne['adr'],
		'X' => $borne['X'],
		'Y' => $borne['Y'],
		'operateur'=>$borne['operateur'],
		'lon' => $borne['lon'],
		'lat' => $borne['lat'],
		'2G' => $borne['2G'],
		'3G' => $borne['3G'],
		'4G' => $borne['4G'],
	    'distance' => distance( $borne,$coords) ,
	    'adresse' => Geocodage($borne['lon'], $borne['lat'])  ];
        $i++;
    }
    return $bornes;
}
function constructionSD2(){
$lecture_donées=json_decode(file_get_contents('DSPE_ANT_GSM_EPSG4326.json'), true);
$i=0;
foreach ($lecture_donées['features'] as $borne){
	$bornes[]=array(
		'id' => $borne['properties']["ADRES_ID"],
		'numcr' => $borne['properties']["NUM_CARTORADIO"],
		'nums' => $borne['properties']["NUM_SUPPORT"],
		'microcell' => $borne['properties']["MICROCELL"],
		'adr' => $borne['properties']["ANT_ADRES_LIBEL"],
		'X' => $borne['properties']["X"],
		'Y' => $borne['properties']["Y"],
		'operateur'=> $borne['properties']["OPERATEUR"],
		'lon' => $borne["geometry"]["coordinates"][0],
		'lat' => $borne["geometry"]["coordinates"][1],
		'2G' => $borne['properties']["ANT_2G"],
		'3G' => $borne['properties']["ANT_3G"],
		'4G' => $borne['properties']["ANT_4G"],
	);
$i++;
}
return $bornes;
}
function proximitéNbis($bornes,$coord,$N,$operator){
    foreach ($bornes as $borne)
    {
    	if ($borne['operateur']==$operator){
        $distances[] = distance($coord, $borne);
        $bornes_op[]=$borne;
    }
    }
    array_multisort($distances, SORT_ASC , $bornes_op);
    $bornes_op= array_slice($bornes_op, 0, $N);
     echo "<table border='1' width='800' cellspacing='0' cellpadding='5' > ";
     echo "<tr style='background-color: red'><th>"; echo "Ordre ";echo "</th><th>"; echo "ID Antenne "; echo "</th><th>";  echo "Distance entre Antenne et coord";echo "</th><th>";echo "Adresse";echo "</th></tr>";
     $ordre=1;
    foreach ($bornes_op as $proche)
    {
        echo "<tr><td>"; echo $ordre ; echo "</td><td>"; echo $proche["id"]; echo "</td><td>";  echo $proche["distance"];echo "</td><td>";echo $proche["adresse"];echo "</td></tr>";
        $ordre++;
    }
    echo "</table>";
}
$coord=array('lon' => (float) $_GET["lon"], 'lat' => (float) $_GET["lat"]);
$bornes=constructionSD2();
$bornes=set_array_adresses_distances2($bornes, $coord);
$top=proximitéNbis($bornes,$coord,$_GET["top"],$_GET["ope"]);
print_r($top);
?>