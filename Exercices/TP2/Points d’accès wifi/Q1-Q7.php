<?php include('/home/chemsou/Bureau/Polytech-2020/S2/PWEB/pw_tps/Helpers/tp2-helpers.php');
    /*	$lines = file($argv[1]);
    	echo count($lines)."\n";
    	$csv = array_map('str_getcsv', file($argv[1]));
        array_walk($csv, function($a) use ($csv) {
        $csv=initAccesspoint($a);
    });
        echo count($csv)."\n";
    print_r($csv);*/
    //Quesion 3
function construction_SD($path){
    $csv_fields_array = file($path);
    $bornes = array();
    foreach ($csv_fields_array as $line)
    {
            $line_array = str_getcsv($line);
            $bornes[]=initAccesspoint($line_array);
    }
    return $bornes;
}
//Question 5
function proximité($bornes,$coord){
    //CONSTRUCTION TABLEAU DISTANCES AVEC TOUT LES PLACES
    echo "Les points d'accès:\n";
    $i=0;
    foreach ($bornes as $borne)
    {
        $distances[] = distance($coord, $borne);
        echo $borne['name']." : ".$distances[$i]." m\n";
        $i++;

    }
    echo "\n";
    $nbr_bornes_200 = 0;
    $minimum = 200;
    $borne_plus_proche = NULL;
    $i=0;
    echo "Les points d'accès à moins de 200 mètres :\n";

    foreach ($bornes as $borne)
    {
        if ($distances[$i] <= 200)
        {
            echo $borne['name']." : ".$distances[$i]." m\n";
            $nbr_bornes_200++;

            if ($distances[$i] < $minimum)
            {
                $minimum = $distances[$i];
                $borne_plus_proche = $borne;
            }
        }
        $i++;
    }
    echo "\n(".$nbr_bornes_200." bornes à moins de 200 m)\n";
    echo "Borne la plus proche : ".$borne_plus_proche['name'].", ".$minimum."m\n\n";
}
//Question 6
function proximitéN($bornes,$coord,$N){
    foreach ($bornes as $borne)
    {
        $distances[] = distance($coord, $borne);
    }
    array_multisort($distances, SORT_ASC , $bornes);
    $proches= array_slice($bornes, 0, $N);
    //print_r($proches);
    $i=0;$j=1;
    foreach ($proches as $proche)
    {
        echo  "la borne numéro  " .  $j . "  est  ".$proche['name']. " et sa distance est ".$distances[$i]."\n";
        $i++; $j++;
    }
}
// Question 7
function Geocodage($lon , $lat){
       $json=smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=".$lon."&lat=".$lat, 0);
       $geocode=json_decode($json , true);
      return $geocode["features"][0]["properties"]["name"];
       }
function set_array_adresses_distances($bornes ,$coords){
     $i=0;
    foreach ($bornes as $borne)
    {
       $bornes[$i]=["name" =>$bornes[$i]['name'],"adr" =>$bornes[$i]['adr'],"lon" =>$bornes[$i]['lon'], "lat" =>$bornes[$i]['lat'] , "distance" => distance( $borne,$coords) , "adresse" => Geocodage($borne['lon'], $borne['lat'])  ];
        $i++;
    }
    return $bornes;
}
/*
$bornes=construction_SD('/home/chemsou/Bureau/Polytech-2020/S2/PWEB/pw_tps/Exercices/TP2/Points d’accès wifi/text1');
$grenet_coord = array('lon' => 5.72752, 'lat' => 45.19102);
echo 'test question 5 : ';
echo "\n";
proximité($bornes , $grenet_coord);
echo 'test question 6: ';
echo "\n";
proximitéN($bornes , $grenet_coord, $argv[1]);
 echo 'test question 7 : ';
 echo "\n";
$bornes=set_array_adresses_distances($bornes , $grenet_coord);
print_r($bornes);
*/

 ?>