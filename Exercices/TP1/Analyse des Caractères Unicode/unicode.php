<!DOCTYPE html>
<html>
    <head>
        <title>Unicode</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="unicode.css">
    </head>

    <body>
        <div class="box">
            <?php
                if(empty($_POST["mot"])){
                echo "il faut saisir un mot";
            }else{
                if (isset($_POST["mot"])){
                    $character = $_POST["mot"];
                    $code = mb_ord($character);
                    $tmp = $code-$code%16;
                    for ($i=0 ; $i<16 ; $i++){
                        $commande = "unicode -d " . ($tmp+$i) . " | head -n 1 | cut -c 8-";
                        $label = exec($commande);
                        if ($tmp+$i == $code){
                            printf("<div class=\"char highlight\" title=\"%s\">%s", $label, mb_chr($tmp + $i));
                        }
                        else{
                            printf("<div class=\"char\" title=\"%s\">%s", $label,  mb_chr($tmp + $i));
                        }
                        printf("<a class=\"id\" href=\'https://util.unicode.org/UnicodeJsps/character.jsp?a=%04x\">", $tmp+$i);
                        printf("U+%04x</a></div>", $tmp+$i);
                    }
                }
                }
            ?>
        </div>
        <form method="post" action="unicode.php">
            <label for="mot">mot à saisir</label> <input type="text" id="mot" name="mot"/> <br />
            <input type="submit" value="submit"/>
        </form>
    </body>
</html>
