<style>
<?php include 'calendar2.css'; ?>
.error {color: #FF0000;}
</style>


<?php echo '<h1>'.'Clendar 2'.'</h1>'; ?>

<?php
$monthErr = $yearErr =$jourErr=$eventErr="";
$month = $year =$jour="0";
$event="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["year"])) {
    $yearErr = "Year is required";
  } else {
    $year = test_input($_POST["year"]);
  }
  
  if (empty($_POST["month"])) {
    $monthErr = "Month is required";
  } else {
    $month = test_input($_POST["month"]);
  }
  if (empty($_POST["jour"])) {
    $jourErr = "Day is required";
  } else {
    $jour = test_input($_POST["jour"]);
  }
  if (empty($_POST["event"])) {
    $eventErr = "Event is required";
  } else {
    $event = test_input($_POST["event"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<p><span class="error">* required field</span></p>
<form method="post" action="calendar2.php">  
  year: <input type="number" name="year">
  <span class="error">* <?php echo $yearErr;?></span>
  <br><br>
  month: <input type="number" name="month">
  <span class="error">* <?php echo $monthErr;?></span>
  <br><br>
  Day: <input type="number" name="jour">
  <span class="error">* <?php echo $jourErr;?></span>
  <br><br>
  Event: <input type="text" name="event">
  <span class="error">* <?php echo $eventErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>








<table>
<?php
if( ($month <0) || ($year <=0) || ($month >12) || ($event=="") || ($jour>31) || ($jour<0)) {echo "Vous devez sélectionner des nombres  cohérents avec des dates et un event valide"; 
}else{
    if ($year!=0 && $month!=0){
    $Nb_days = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
    switch($month){
        case 1:
          $strmonth="janvier";
          break;
        case 2:
          $strmonth="février";
          break;
        case 3:
          $strmonth="mars";
          break;
        case 4:
          $strmonth="avril";
          break;
        case 5:
          $strmonth="mai";
          break;
          case 6:
          $strmonth="juin";
          break;
          case 7:
          $strmonth="juillet";
          break;
          case 8:
          $strmonth="aout";
          break;
          case 9:
          $strmonth="septembre";
          break;
          case 10:
          $strmonth="octobre";
          break;
          case 11:
          $strmonth="novembre";
          break;
          case 12:
          $strmonth="décembre";
          break;


    }
    echo "Pour: ".$strmonth.'  '.$year." on a :";
    for($x = 1; $x <=$Nb_days; $x++){               
        echo "<tr>";

        $date = $year.'-'.$month.'-'.($x);   
        $unixTimestamp = strtotime($date);          
        $dayOfWeek = date("l", $unixTimestamp); 
         if($x%7==0){                      
            echo '<br>';
            echo "<tr>";
        }

        if($x==$jour){
           $info=$event;
        } else{$info = "";}
        echo '<td>'.$x.'</td>';
        echo '<td>'.$dayOfWeek.'</td>';
        echo '<td>'.$info.'</td>';


    }
}
}
?>

</table>
