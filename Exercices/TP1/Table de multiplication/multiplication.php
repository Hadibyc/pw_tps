<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="multiplication.css">
    <title>Table de multiplication</title>
</head>
<body>
    <table>
    <?php 
    $lignes = $_REQUEST["lignes"];
    $colonnes = $_REQUEST["colonnes"];
    if( ($lignes <=0) || ($colonnes <=0) || ($colonnes >10) || ($lignes > 10)) {echo "Vous devez sélectionner des nombre entre 1 et 10"; 
     echo'<a href="multiplication.html" > clique ici</a> pour revenir à la page précedente .';
}
    else{
    if (empty($lignes)) $lignes = 10;
    if (empty($colonnes)) $colonnes = 10;
    if(isset($lignes) && isset($colonnes)){
        for($i=0; $i<=$lignes; $i++){
            print("<tr>");
            for($j=0; $j<=$colonnes; $j++){
                $result = $j*$i;
                if ($i == 0) {
                    print("<th> $j </th>");
                } else if ($j == 0) {
                    print("<th> $i </th>");
                } else {
                    print("<td> $result </td>");
                }
            }   
            print("</tr>");
        }   
    }
       }    
    ?>
    </table>
    
</body>
</html>