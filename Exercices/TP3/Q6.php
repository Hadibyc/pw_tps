<!DOCTYPE HTML>
<html>
    <head>
        <title>Webservice API search</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 10px;
  text-align: left;
}
th {
  background-color: yellow;
}


</style>

    </head>
    <body>
      <h1>Collections Movie Database </h1>
        <form method="get" action="Q6.php">
            <label for="id">Collection Name :</label>
            <input type="text" id="id" name="id" required />
      <br />
            <br />
            
            <input type="submit" value="Trouver la collection " />
             <br />
             <br />
        </form>
<?php
require_once("tp3-helpers.php");
function collection_details($name){
	$url="search/collection";
	$query=$name;
	$collection=tmdbget($url ,array("query"=>$query));
	$collection=json_decode($collection,true);
	if($collection["total_results"]==0){ echo "<h3> Sorry , this name does not match to any collection </h3>";}
	else{
		foreach($collection["results"] as $under_collection){
		echo "<center><h3> "; echo $under_collection["name"]; echo" </h3> </center>";
		$url_bis="collection/".$under_collection["id"];
		$collection_bis=tmdbget($url_bis);
		$collection_bis=json_decode($collection_bis,true);
		echo "<center><table>
			 <tr>
			    <th>ID du film</th> 
			    <th>Nom du film</th>
			    <th >Date De Sortie</th>
			  </tr>";
		foreach($collection_bis['parts'] as $movies){
				echo "
			  <tr>
			    <td>";echo $movies['id'] ; echo"</td>
			    <td>";echo $movies['title'] ; echo"</td>
			    <td>";echo $movies['release_date'] ; echo"</td>			    
			  </tr>";
			 
			 
	}
	echo "</table></center>";
	}
}

}
if(isset($_GET['id'])){
  collection_details($_GET['id']);
}
?>
