
<!DOCTYPE HTML>
<html>
    <head>
        <title>Webservice API search</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 10px;
  text-align: left;
}
#t01 {
  width: 10%;    
  background-color: silver;
}
</style>
    </head>
    <body>
    	<h1>Webservice API search</h1>
        <form method="get" action="Q4.php">
            <label for="id">ID du film :</label>
            <input type="number" step="1" value="105"  min="1" id="id" name="id" required />
 			<br />
            <br />
            
            <input type="submit" value="Trouver les informations du film " />
             <br />
             <br />
        </form>

        <?php
        require_once("tp3-helpers.php");
        function affichage($langue){
        if (isset($_GET["id"]) ){
        	$information =array();
        $content =tmdbget("movie/".$_GET["id"],['language' => $langue ]);
        $content=json_decode($content,true);
        if(in_array ($_GET["id"],$content)){
        if($content["original_title"]!= null){$information[0]=( $content["original_title"]);}else{$information[0]='';}
        if($content["tagline"]!= null){$information[1]=( $content["tagline"]);} else{$information[1]='';}
        if($content["overview"]!= null){$information[2]=( $content["overview"]);}else{$information[2]='';}
        $lien=$content["homepage"];
        if($lien!= null){
        $information[3]=$lien;
    	}  else{$information[3]=' ';}
		}else $information[0]="cette id ne correspond à aucun film ";
		return $information;
        }
    }
    if (isset($_GET["id"]) ){
    	$information_VO=affichage('');
    	$information_fr=affichage('fr');
    	$information_en=affichage('en');   	
if ($information_VO[0]!="cette id ne correspond à aucun film "){
echo "<table>
 <tr>
    <th></th>
    <th id='t01'>Version Originale</th>
    <th id='t01'>Version Française </th> 
    <th  id='t01'>Version Anglaise </th>
  </tr>
  <tr>
    <th id='t01'>Original title </th>
    <td>"; echo $information_VO[0]; echo "</td>
    <td>"; echo $information_fr[0]; echo"</td>
    <td>";echo $information_en[0]; echo"</td>
  </tr>
  <tr>
    <th  id='t01'>Tagline </th>
    <td>"; echo $information_VO[1]; echo"</td>
    <td>"; echo $information_fr[1];echo"</td>
    <td>"; echo $information_en[1];echo"</td>   
  </tr>
   <tr>
    <th  id='t01'>Overview </th>
    <td>"; echo $information_VO[2];echo"</td>
    <td>"; echo $information_fr[2];echo"</td>
    <td>"; echo $information_en[2];echo"</td>   
  </tr>
    <tr>
    <th  id='t01'>Link </th>";
    $string="Link to the official website ";
    if ($information_VO[3]==' '){$string='';}
    echo "<td><a href='$information_VO[3]' > $string </a></td>
    <td><a href='$information_VO[3]' > $string</a></td>
    <td><a href='$information_VO[3]' > $string </a></td>
 
  </tr>
</table>";
}else{
echo $information_VO[0];
}
    }
        ?>
    </body>
</html>
